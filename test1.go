package main

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"fmt"
	"time"
)

// TODO make this a useful test suite
// TODO split path, queue, graph into their own files

var _ = Describe("Edmonds-Karp", func() {
	var ()

	BeforeEach(func() {
	})

	Describe("graphs!", func() {
		Context("stuff", func() {
			/*
			It("should build and print!", func() {
				// wikipedia edmonds karp
				graphy := newGraph("bobster", 7)
				//graphy.toStr()
				graphy.add(0, 1, 3)
				graphy.add(0, 3, 3)
				graphy.add(1, 2, 4)
				graphy.add(2, 3, 1)
				graphy.add(2, 4, 2)
				graphy.add(3, 4, 2)
				graphy.add(3, 5, 6)
				graphy.add(4, 1, 1)
				graphy.add(4, 6, 1)
				graphy.add(5, 6, 9)
				ans := graphy.edmondsKarp(0, 6)
				Expect(ans).To(Equal(5.0))
			})

			It("should work on the geeksforgeeks example (no back edges)", func() {
				// geeksforgeeks max flow
				//graphy.toStr()
				graphy := newGraph("online example. expect 23", 6)
				graphy.add(0, 1, 16)
				graphy.add(0, 2, 13)
				graphy.add(1, 2, 10)
				graphy.add(1, 3, 12)
				graphy.add(2, 1, 4)
				graphy.add(2, 4, 14)
				graphy.add(3, 2, 9)
				graphy.add(3, 5, 20)
				graphy.add(4, 3, 7)
				graphy.add(4, 5, 4)
				ans := graphy.edmondsKarp(0, 5)
				Expect(ans).To(Equal(23.0))
			})
			
			It("should have a working unionfind!", func() {
				uf := newUnionfind(10)
				uf.Union(2,5)
				uf.Find(5)
				uf.Union(8,2)
				uf.Union(8,2)
				uf.Union(2,1)
				uf.Union(0,4)
				uf.Union(0,5)
				uf.Print()
				Expect(2).To(Equal(2))
			})
			*/
			It("should build and print!", func() {
				vertices := 125
				edges := 2
				partitions := 2.0
				maxRand := 36
				minRand := 8
				fmt.Println("Begin test with following criteria")
				fmt.Printf("Vertices: %d, Edges: %d, Partitions: %d, maxRand: %d, minRand: %d\n",vertices,edges,(int)(partitions),maxRand,minRand)
				graphy := newGraph("blank", 1)
				size := 25
				time_arr := make([]int64, size)
				flow_arr := make([]float64, size)
				for i:=0; i<size; i++{
					graphy = newGraph("graph_rand", vertices)
					graphy = graphy.MakeBipartite(partitions, edges, maxRand, minRand)
					start := time.Now()
					//graphy.toStr()
					ans := graphy.edmondsKarp(0, vertices-1)
					elapsed := time.Since(start)
					//fmt.Println(elapsed.Milliseconds())
					time_arr[i]=elapsed.Milliseconds()
					//fmt.Println(ans)
					flow_arr[i] = ans
					//fmt.Prinln(time_arr[i])
					//fmt.Println(flow_arr[i])
				}
				for i:=0; i<size; i++{
					fmt.Println(time_arr[i])
				}
				fmt.Println("--------")
				for i:=0; i<size; i++{
					fmt.Println((int)(flow_arr[i]))
				}
					//graphy.toStr()

				Expect(5.0).To(Equal(5.0))
			})
		})
	})
})
