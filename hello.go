package main

import (
	"fmt"
	"testing"
)

func main() {
	fmt.Println("Test case driver begins here:")
	TestGraph(&testing.T{})
}
