package main

import "fmt"

// Unionfind is a union-find data structure
type Unionfind struct {
	vals []int
	size []int
}

// creates a Unionfind
func newUnionfind(lenth int) *Unionfind {
	unionfind := Unionfind{}
	a := make([]int, lenth)
	b := make([]int, lenth)
	for i:= 0; i < len(a); i++{
		a[i] = -1
		b[i] = 1
	}
	unionfind.vals = a
	unionfind.size = b
	return &unionfind
}

// unions the two sets that vertices u and v are in
func (uf *Unionfind) Union(u int, v int) {
	a := uf.Find(u)
	b := uf.Find(v)
	c := uf.size[a]
	d := uf.size[b]
	if a !=b {
		tempSize := uf.size[a] + uf.size[b]
		if c > d {
			uf.vals[b] = a
			uf.vals[a] = -1
			uf.size[a] = tempSize
			uf.size[b] = -7
		}else{
			uf.vals[a] = b
			uf.vals[b] = -1
			uf.size[b] = tempSize
			uf.size[a] = -7
		}
	}
	return
}

// finds the parent of the vertex u
// it does this by getting the parent of the parent of u and flattening the parents as needed
func (uf *Unionfind) Find(u int) (int) {
	if uf.vals[u] == -1 {
		return u
	}else{
		uf.vals[u] = uf.Find(uf.vals[u])
		return uf.vals[u]
	}
}

// creates a debug print statements for the unionfind
// TODO update this to a string builder
func (uf *Unionfind) Print() {
	fmt.Println("Begin printing uf")
	for i:= 0; i < len(uf.vals); i++{
		fmt.Printf("i: %d, val: %d, size: %d\n", i, uf.vals[i], uf.size[i])
	}
	fmt.Println("Done printing uf")
}
