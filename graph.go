package main

import (
	"fmt"
	"math"
	"math/rand"
)

const (
	// ADD adds an instruction to a list
	ADD = iota
	// REMOVE removes an instruction from a list
	REMOVE = iota + 1
)

// TODO add real documentation
// TODO add real error detection
// TODO add a readme for dependencies (just ginkgo for testing and go itself)
// TODO detect vertices visiting themself
// TODO detect edges with negative weights

// Graph is a graph representation
type Graph struct {
	Name string      // the name
	Dim  int         // the n value for the size N x N
	C    [][]float64 // capacity of edges matrix
	E    [][]float64 // adjacency of edges matrix
	F    [][]float64 // residual capacity of edges matrix
}

// creates a new blank graph
func newGraph(name string, dim int) *Graph {
	g := Graph{}
	g.Name = name
	g.Dim = dim
	c := make([][]float64, dim)
	for i := 0; i < dim; i++ {
		c[i] = make([]float64, dim)
		for x := 0; x < dim; x++ {
			c[i][x] = 0
		}
	}
	e := make([][]float64, dim)
	for i := 0; i < dim; i++ {
		e[i] = make([]float64, dim)
		for x := 0; x < dim; x++ {
			e[i][x] = 0
		}
	}
	f := make([][]float64, dim)
	for i := 0; i < dim; i++ {
		f[i] = make([]float64, dim)
		for x := 0; x < dim; x++ {
			f[i][x] = 0
		}
	}
	g.C = c
	g.E = e
	g.F = f
	return &g
}

// "prints" a representation of the graph for debugging
// TODO make this an actual toStr rather than some prints
func (g Graph) toStr() {
	fmt.Println(g.Name)
	fmt.Println(g.Dim)
	fmt.Println("C")
	fmt.Println(g.C)
	fmt.Println("E")
	fmt.Println(g.E)
	fmt.Println("F")
	fmt.Println(g.F)
}

// preProcess augments a graph transforming all vertices of the form "A->B AND B->A" into "A->B AND B->C AND C->A"
// this is necessary because my implementation of edmonds-karp initially assumed that graphs could only contain A->B XOR B->A
// it augments the graph and transforms it into a valid format
func (g *Graph) preProcess(s, t int) *Graph {
	// makes a square 2d array of g's dim
	dim := g.Dim
	visited := make([][]float64, dim)
	for i := 0; i < dim; i++ {
		visited[i] = make([]float64, dim)
		for x := 0; x < dim; x++ {
			visited[i][x] = 0
		}
	}
	ctr := 0
	c := dim
	instrList := newInstructionList()
	for i := 0; i < dim; i++ {
		for j := 0; j < dim; j++ {
			// if both i and j have an outgoing edge to eachother, do the transform on them
			if g.E[i][j] == 1 && g.E[j][i] == 1 && visited[i][j] != 1 || visited[j][i] != 1 {
				visited[i][j] = 1
				visited[j][i] = 1
				if !(visited[i][j] == 1 || visited[j][i] == 1) {
					c = dim + ctr
					ctr++
					instrList.append(ADD, g.C[j][i], j, c)
					instrList.append(ADD, g.C[j][i], c, i)
					instrList.append(REMOVE, 0, j, i)
				}
			}
		}
	}

	// copies g into gExtended
	gExtended := newGraph(g.Name+" Extended", dim+ctr)
	for i := 0; i < dim; i++ {
		for j := 0; j < dim; j++ {
			gExtended.E[i][j] = g.E[i][j]
			gExtended.F[i][j] = g.F[i][j]
			gExtended.C[i][j] = g.C[i][j]
		}
	}

	instrList, kind, capacity, u, v, err := instrList.pop()
	for err == nil {
		if kind == ADD {
			// adds the edge from U->V to the graph completely
			gExtended.E[u][v] = 1
			gExtended.C[u][v] = capacity
			gExtended.F[u][v] = 0
		} else if kind == REMOVE {
			// removes the edge from U->V from the graph completely
			gExtended.E[u][v] = 0
			gExtended.C[u][v] = 0
			gExtended.F[u][v] = 0
		} else {
			// TODO make me an error
			fmt.Printf("umm not expecting this else statement in preProcessing")
		}
		// readies the next instruction
		instrList, kind, capacity, u, v, err = instrList.pop()
	}
	return gExtended
}

// finds the augmenting paths using breadth first search. this is the 4th edition of it
func (g *Graph) bfs4(s, t int) (float64, *Path) {
	// creates a new blank path
	p := newPath(g.Dim)

	// creates the queue for the bfs and adds the start node to it
	ll := newLinkedList()
	ll.append(0)
	p.Cost[0] = 0
	// sets the hop number of all items to infinity
	for i := 1; i < len(p.Cost); i++ {
		p.Cost[i] = math.Inf(1)
	}
	// while the queue is nonempty, conduct bfs from the head
	for ll.len > 0 {
		// pops the head of the queue
		// TODO check this error and return a real error with fmt.Errorf()
		ll, u, _ := ll.pop()
		p.Visited[u] = true
		// adds all unvisited neighbors to the queue and marks visited
		// regardless of if the neighbor is visited, if it has a used incoming edge from the neighbor or an outgoing edge to the neighbor
		// update the cost and parent of the neighbor if it would be shorter to get to it from the current node
		for i := 0; i < g.Dim; i++ {
			// gets remaining capacity from U to I after removing the current flow from U to I
			remainingFlow := g.C[u][i] - g.F[u][i]
			// gets remaining capacity from I to U after removing the current flow from I to U
			remainingFlowReverse := g.C[i][u] - math.Abs(g.F[i][u])
			// if there is an edge either way from U-I and there exists potential for flow to go across it, consider updating the parents and adding the neighbor to the queue
			if g.E[u][i] != 0 && remainingFlow != 0 || g.E[i][u] != 0 && remainingFlowReverse != g.C[i][u] {
				// if i has more hops than u+1, update i's cost and parent
				if p.Cost[i] > p.Cost[u]+1 {
					p.Cost[i] = p.Cost[u] + 1
					p.Parent[i] = u
				}
				// regardless of if parent is updated, if i is not visited, add it to the back of the queue and mark visited
				if p.Visited[i] == false {
					p.Visited[i] = true
					ll.append(i)
				}
			}
		}
	}

	// instantiate the minimum value to infinity
	mins := math.Inf(1)
	// set the current node to the target node (sink)
	v := t

	//while current node is not the parent, set node equal to parent and compare the remaining edge capacity to min. update accordingly
	// returns min at the end
	for v != s {
		// sets u to the parent of the current node
		u := p.getParent(v)
		// if the parent does not have a parent (-1) then we return a fake error code, -23
		// TODO make this a real fmt.Errorf()
		if u == -1 {
			return -23, &p
		}
		remainingFlow := -5.0
		// if U not connected to V but V connected to U
		if g.E[u][v] == 0 && g.E[v][u] == 1 {
			remainingFlow = g.C[v][u] - math.Abs(g.F[v][u])
		} else if g.E[u][v] == 1 && g.E[v][u] == 0 {
			//if U connected to V but V not connected to U
			remainingFlow = g.C[u][v] - math.Abs(g.F[u][v])
		} else {
			// This should never occur if we are running the intermediate vertex algorithm
			// eg: if we have A->B and B->A, we would replace B->A with two edges and one vertex, C, B->C, C->A
			fmt.Println("Unexpected value in remaining flow. This likely arises from U being connected to V and V being connected to U")
		}

		mins = min(remainingFlow, mins)
		v = u
	}
	return mins, &p
}

// returns lesser of a, b
func min(a, b float64) float64 {
	if a < b {
		return a
	}
	return b
}

// adds an edge from u to v with an associated capacity
func (g *Graph) add(u, v int, capacity float64) {
	g.C[u][v] = capacity
	g.F[u][v] = 0
	g.E[u][v] = 1
}

// returns the max flow from a source to a target on a given graph
func (g *Graph) edmondsKarp(s, t int) (flow float64) {
	// initialize flow to 0
	flow = 0
	m := -1.0
	// do while we find an augmenting path with weight > 0
	for m != 0 {
		// finds the value of the minimum edge on the shortest-hop augmenting path and said path
		m, path := g.bfs4(s, t)

		// break if m is zero or a hacky -23 error value
		// TODO update me from 23
		if m == 0 || m == -23 {
			if m == 0 {
				//fmt.Printf("em: %f\n", m)
			}
			break
		}
		// flow is incremented by the value of the augmenting path found in the bfs
		flow = flow + m
		// sets v equal to the target (sink)
		v := t
		// update the flow and residual values for each parent and child in the path found in the bfs
		// while current node is not the source, get its parent and update the residual and flow values
		for v != s {
			u := path.getParent(v)
			// if node does not have a parent, return a bad error code
			// TODO make me a fmt.Errorf() error
			if u < 0 {
				return -1
			}
			g.F[u][v] = g.F[u][v] + m
			g.F[v][u] = g.F[v][u] - m
			// sets current node to its parent
			v = u
		}
	}
	// debug print
	//fmt.Printf("Flow is doneeee: %f\n", flow)
	return flow
}

// adds bipartiteness to a graph
func (g *Graph) MakeBipartite(layers float64, conns, maxRand, minRand int) (*Graph) {
	var b int
	b = (int) (math.Ceil(((float64)(g.Dim - 2)) / layers))
	// for every vertex in the graph
	//fmt.Printf("dim: %d\n",g.Dim)
	for i := 0; i< g.Dim; i++{
		//fmt.Printf("i: %d\n",i)
		offsetGroup := (((i-1)/b) + 1)*b
		//fmt.Printf("osg: %d\n",offsetGroup)

		if i == 0{
			//fmt.Println("Branch i==0")
			//if S connect to all guys in first layer
			for ctr := 1; ctr <= b; ctr++ {
				v := (float64)(rand.Intn((maxRand-minRand)+1) + minRand)
				g.add(0, ctr, v)
			}
		}else if i >= g.Dim-1{
			// if T break
			//fmt.Println("Branch i>= g.Dim-1")
			break
		}else if offsetGroup + b > g.Dim -1{
			//fmt.Println("osg + b too big")
			v := (float64)(rand.Intn((maxRand-minRand)+1) + minRand)
			g.add(i,g.Dim-1, v)
		}else if i > 0 && i != g.Dim-1{
			// set the offset it can target to
			max := b
			// if it is the last layer, we might have a smaller vertex set (25 into groups of 8 is 1,8,8,7,1)
			if b + i >= g.Dim-1{
				max = (g.Dim - 2) % (int) (layers)
			}
			min := 1
			tempCons := conns
			if conns > max {
				tempCons = conns
			}
			if conns > b{
				tempCons = b
			}
			locs := make([]int, tempCons)
			//creates a connection to a unique target vertex
			for j := 0; j<tempCons;j++{
				var randTemp int
				inArr := 1
				// determines uniqueness
				abc := 0
				for inArr == 1{
					randTemp = rand.Intn((max-min)+1) + min
					inArr = 0
					abc++
					if abc > 30{
						break
					}
					for k:=0;k<tempCons;k++{
						if locs[k] == randTemp {
							inArr = 1
							break
						}
					}
				}
				locs[j] = randTemp
			}
			// sets edges to exist
			for j := 0; j<tempCons;j++{
				v := (float64)(rand.Intn((maxRand-minRand)+1) + minRand)
				g.add(i, offsetGroup + locs[j], v)

			}
			
		}
		/*
		else if i == g.Dim - 1{
			//connect all guys in last layer to T
				c := (g.Dim-2) % b
				//fmt.Printf("b2: %d\nc: %d\n",b,c)
				if c == 0{
					c = b
				}
				for ctr := c; ctr > 0; ctr--{
					//fmt.Printf("ctr: %d\n",ctr)
					v := (float64)(rand.Intn((maxRand-minRand)+1) + minRand)
					g.add((g.Dim-1)-ctr, g.Dim-1, v)
				}
		}
		*/

	}
	return g
}

// Some initial project notes:

// Max flow did not account for reverse edges for a while
// many different iterations of max flow
// initially thought shortest-path was total sum, not number of hops
// initially had only solved for edges where both A->B and B->A not allowed, but an augmenting algorithm solved that
