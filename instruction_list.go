package main

import "fmt"

// TODO maybe create a node interface so that I don't have two different linked list implementations

// Instruction is a node
type Instruction struct {
	kind     int     // the action to do
	capacity float64 // the capacity of the edge
	u        int     // the source vertex
	v        int     // the end vertex
	next     *Instruction
}

// InstructionList represents a linked list of instructions
type InstructionList struct {
	head *Instruction
	tail *Instruction
	len  int
}

// creates an instruction
func newInstruction(kind int, capacity float64, u, v int) *Instruction {
	instr := Instruction{}
	instr.kind = kind
	instr.capacity = capacity
	instr.u = u
	instr.v = v
	instr.next = nil
	return &instr
}

// creates a blank InstructionList
func newInstructionList() *InstructionList {
	ll := InstructionList{}
	ll.head = nil
	ll.tail = nil
	ll.len = 0
	return &ll
}

// removes the first item in the queue (FIFO), returns the new list pointer as well as the values and an error
func (ll *InstructionList) pop() (*InstructionList, int, float64, int, int, error) {
	if ll.len == 0 {
		return ll, -1, -1, -1, -1, fmt.Errorf("no items to pop in ll")
	}
	kind := ll.head.kind
	capacity := ll.head.capacity
	u := ll.head.u
	v := ll.head.v
	ll.head = ll.head.next
	ll.len--
	return ll, kind, capacity, u, v, nil
}

// adds an item to the back of the queue (FIFO)
func (ll *InstructionList) append(kind int, capacity float64, u, v int) {
	instr := newInstruction(kind, capacity, u, v)
	if ll.len == 0 {
		ll.len = ll.len + 1
		ll.head = instr
		ll.tail = nil
	} else if ll.len == 1 {
		ll.len++
		ll.head.next = instr
		ll.tail = instr
	} else {
		ll.len++
		ll.tail.next = instr
		ll.tail = instr
	}
}

// creates a debug print statements for the InstructionList
// TODO update this to a string builder
func (ll *InstructionList) print() {
	fmt.Println("Begin printing ll")
	temp := ll.head
	for temp != nil {
		fmt.Println(temp.kind)
		fmt.Println(temp.u)
		fmt.Println(temp.v)
		temp = temp.next
	}
	fmt.Println("Done printing ll")
}
