package main

import "fmt"

// Node is a node
type Node struct {
	value int
	next  *Node
}

// LinkedList represents a linked list
type LinkedList struct {
	head *Node
	tail *Node
	len  int
}

// creates a node
func newNode(value int) *Node {
	node := Node{}
	node.value = value
	node.next = nil
	return &node
}

// creates a blank linked list
func newLinkedList() *LinkedList {
	ll := LinkedList{}
	ll.head = nil
	ll.tail = nil
	ll.len = 0
	return &ll
}

// removes the first item in the queue (FIFO), returns the new list pointer as well as the val and an error
func (ll *LinkedList) pop() (*LinkedList, int, error) {
	if ll.len == 0 {
		return ll, -1, fmt.Errorf("no items to pop in ll")
	}
	val := ll.head.value
	ll.head = ll.head.next
	ll.len--
	return ll, val, nil
}

// adds an item to the back of the queue (FIFO)
func (ll *LinkedList) append(val int) {
	node := newNode(val)
	if ll.len == 0 {
		ll.len = ll.len + 1
		ll.head = node
		ll.tail = nil
	} else if ll.len == 1 {
		ll.len++
		ll.head.next = node
		ll.tail = node
	} else {
		ll.len++
		ll.tail.next = node
		ll.tail = node
	}
}

// creates a debug print statements for the linked list
// TODO update this to a string builder
func (ll *LinkedList) print() {
	fmt.Println("Begin printing ll")
	temp := ll.head
	for temp != nil {
		fmt.Println(temp.value)
		temp = temp.next
	}
	fmt.Println("Done printing ll")
}
