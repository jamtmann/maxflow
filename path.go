package main

import "math"

// Path is a path
type Path struct {
	Dim     int       //the dimension of the arrays
	Cost    []float64 // the cost of the vertex
	Parent  []int     // the parent of the node
	Visited []bool    // if each vertex is visited
}

// creates a new path
func newPath(dim int) (p Path) {
	p.Dim = dim
	p.Cost = make([]float64, dim)
	p.Parent = make([]int, dim)
	p.Visited = make([]bool, dim)
	for i := 0; i < dim; i++ {
		p.Cost[i] = math.Inf(1)
		p.Parent[i] = -1
		p.Visited[i] = false
	}
	return p
}

// gets the parent of a vertex in a path
func (p Path) getParent(u int) int {
	if u < p.Dim {
		return p.Parent[u]
	}
	// a garbage error value
	// TODO give it a real fmt.Errorf() value
	return -5
}
